from django.conf import settings
from django.contrib import messages
from django.core import mail
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string

from eventex.subscriptions.forms import SubscriptionForm
from eventex.subscriptions.models import Subscription


def subscribe(request):
    if request.method == 'POST':
        return create(request)
    else:
        return new(request)


def create(request): # salvar a inscricao
    form = SubscriptionForm(request.POST)

    # form.full_clean() # transforma strings em objetos python de alto nível 2 dicionario 1 valid outro de erro
    # o form.is_valid() internamente chama o full_clean

    if not form.is_valid():
        return render(request, 'subscriptions/subscription_form.html',
                      {'form': form})

    subscription = Subscription.objects.create(**form.cleaned_data)

    request.session['subscription_pk'] = subscription.pk

    _send_mail('Confirmação de inscrição',
               settings.DEFAULT_FROM_EMAIL,
               subscription.email,
               'subscriptions/subscription_email.txt',
               {'subscription': subscription})

    # _send_mail('Confirmação de inscrição',
    #            settings.DEFAULT_FROM_EMAIL,
    #            form.cleaned_data['email'],
    #            'subscriptions/subscription_email.txt',
    #            form.cleaned_data)

    # messages.success(request, 'Inscrição realizada com sucesso!')

    # return HttpResponseRedirect('/inscricao/{}/'.format(subscription.pk))
    # return HttpResponseRedirect('/inscricao/obrigado/')

    return HttpResponseRedirect('/inscricao/{}/'.format(subscription.hashid))


def new(request):
    return render(request, 'subscriptions/subscription_form.html',
                  {"form": SubscriptionForm()})


# def detail(request, pk):
def detail(request, hashid):
    try:
        subscription = Subscription.objects.get(hashid=hashid)
    except Subscription.DoesNotExist:
        raise Http404

    return render(request, 'subscriptions/subscription_detail.html',
                  {'subscription': subscription})


# O "_" antes do send_mail é uma sinalização para os programadores dizendo que eu coloquei uma
# sinalização que indica para o programador não acessar diretamente a função pois não tem compromisso
# de dar manutenção
def _send_mail(subject, from_, to, template_name, context):
    body = render_to_string(template_name, context)
    mail.send_mail(subject, body, from_, [from_, to])


# def thank_you(request):
#     if 'subscription_pk' not in request.session:
#         return HttpResponseRedirect('/inscricao/')
#
#     subscription = get_object_or_404(Subscription, pk=request.session['subscription_pk'])
#     del request.session['subscription_pk']
#     return render(request, 'subscriptions/subscription_detail.html', {'subscription': subscription})


#  # return HttpResponse()
# mail.send_mail('Subject',
#                     'Message',
#                     'sener@email.com',
#                     ['visitor@email.com'])
